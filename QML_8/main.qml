import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    Rectangle{
        id:main_rec
        width:parent.width
        height:300

    SwipeView {
        id: view
        currentIndex: 1
        anchors.fill: parent

            Item {
                id: firstPage
                Rectangle{
                    anchors.margins: 50
                    anchors.fill: parent
                    color:"red"
                }
            }
            Item {
                id: secondPage
                Rectangle{
                    anchors.margins: 50
                    anchors.fill: parent
                    color:"yellow"
                }
            }
            Item {
                id: thirdPage
                Rectangle{
                    anchors.margins: 50
                    anchors.fill: parent

                    color:"green"
                }
            }
        }
    }
    PageIndicator {
        id: indicator
        count: view.count
        currentIndex: view.currentIndex
        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
