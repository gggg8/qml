import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow {
    visible: true
    width: 350
    height: 500

    Rectangle {
        width: parent.width
        height: parent.height

        ColumnLayout {
            anchors.centerIn: parent

            TextField {
                id: username
                placeholderText: "Username"
                width: 200
                height: 30
            }

            TextField {
                id: pass
                placeholderText: "Password"
                width: 200
                height: 30
                echoMode: TextInput.Password
            }

            RowLayout {

                Button {
                    text: "Login  "
                    onClicked: {

                        console.log("Username:", username.text);
                        console.log("Password:", pass.text);
                    }
                }

                Button {
                    text: "Clear  "
                    onClicked: {
                        username.text = "";
                        pass
                        .text = "";
                    }
                }
            }
        }
    }
}
