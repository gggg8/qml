import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Window {
    width: 320
    height: 480
    visible: true

    property string password: ""

    ColumnLayout {
        anchors.centerIn: parent

        Rectangle {
            id: rec_display

            width: 200
            height: 80

            ColumnLayout {
                anchors.centerIn: parent
                spacing: 5

                Text {
                    text: "Enter your password:"
                }

                Row {
                    Layout.alignment: Qt.AlignCenter
                    Repeater {
                        model: 6

                        Label {
                            font.pixelSize: 30
                            text: "*"

                            color: index < password.length ? "black" : "lightgray"
                        }
                    }
                }
            }
        }

        Rectangle {
            width: 200
            height: 250

            GridLayout {
                columns: 3
                columnSpacing: 5
                rowSpacing: 5

                anchors.fill: parent

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "1"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "2"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "3"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "4"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "5"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "6"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "7"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "8"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "9"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "0"

                    onClicked: password += text
                }

                Button {
                    implicitWidth: 50
                    implicitHeight: 50
                    text: "Clear"

                    onClicked: password = ""
                }
            }
        }

        Rectangle {
            width: 220
            height: 40

            Button {
                implicitWidth: 50
                implicitHeight: 50
                text: "Log in"

                onClicked: {
                    console.log("Password:", password)
                }
            }
        }
    }
}
