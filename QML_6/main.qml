import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15

ApplicationWindow {
    visible: true
    width: 400
    height: 600
    title: "StackView"

    header: ToolBar {
        id: page_header
        height: 40

        Row {
            spacing: 10

            Button {
                id: backButton
                text: "Back"
                visible: stackView.depth > 1
                onClicked: stackView.pop()

            }
        }
    }
    StackView {
        id: stackView
        initialItem: redPage
        anchors.fill: parent

        onCurrentItemChanged: {
            console.log("StackView Depth:", stackView.depth);
            backButton.visible = stackView.depth > 1;
            console.log("Back Button Visibility:", backButton.visible);
            header_page_text.text = stackView.currentItem ? stackView.currentItem.itemName : "Home";
        }
        Component {
            id: redPage
            Rectangle {
                width: stackView.width
                height: stackView.height
                color: "red"
                Row {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    Button {
                        text: "Yellow"
                        onClicked: stackView.push(yellowPage)
                    }
                    Button {
                        text: "Green"
                        onClicked: stackView.push(greenPage)
                                       }
                }
            }
        }
        Component {
            id: greenPage
            Rectangle {
                width: stackView.width
                height: stackView.height

                color: "green"

                Row {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right

                    Button {
                        text: "Yellow"
                        onClicked: stackView.push(yellowPage)
                    }
                    Button {
                        text: "Red"
                        onClicked: stackView.push(redPage)
                                       }
                }
            }
        }
        Component {
            id: yellowPage
            Rectangle {
                width: stackView.width
                height: stackView.height

                color: "yellow"

                Row {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right

                    Button {
                        text: "Green"
                        onClicked: stackView.push(greenPage)
                    }
                    Button {
                        text: "Red"
                        onClicked: stackView.push(redPage)
                                       }

                }
            }
        }
    }
}
