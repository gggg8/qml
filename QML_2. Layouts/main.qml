import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow {
    visible: true
    width: 540
    height: 880
    title: "Footer Example"

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true

            Rectangle {
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    text: "Header Content 1"
                    anchors.centerIn: parent
                }
            }
            Rectangle {
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    text: "Header Content 2"
                    anchors.centerIn: parent
                }
            }

            Rectangle {
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    text: "Header Content 3"
                    anchors.centerIn: parent
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "lightgreen"
            Text {
                text: "Content 2"
                anchors.centerIn: parent
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Rectangle {
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    text: "Footer Content 1"
                    anchors.centerIn: parent
                }
            }


            Rectangle {
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    text: "Footer Content 2"
                    anchors.centerIn: parent
                }
            }

            Rectangle {
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    text: "Footer Content 3"
                    anchors.centerIn: parent
                }
            }
        }
    }
}
