import QtQuick 2.15
import QtQuick.Window 2.2

Window {
    visible: true
    width: 800
    height: 800
    color: white



My_Comp{
    id:recCenter
    color:"green"
    width: 200
    height: 200
    anchors.centerIn: parent
}
My_Comp{
    id:head
    color:"red"
    width: 50
    height: 50
    anchors.bottom: recCenter.top
    anchors.horizontalCenter: recCenter.horizontalCenter
}
My_Comp{
    id:leg1
    color:"red"
    width: 50
    height: 30
    anchors.top: recCenter.top
    anchors.right: recCenter.left
}
My_Comp{
    id:leg2
    color:"red"
    width: 50
    height: 30
    anchors.bottom: recCenter.bottom
    anchors.right: recCenter.left
}
My_Comp{
    id:leg3
    color:"red"
    width: 50
    height: 30
    anchors.bottom: recCenter.bottom
    anchors.left: recCenter.right
}

My_Comp{
    id:leg4
    color:"red"
    width: 50
    height: 30
    anchors.top: recCenter.top
    anchors.left: recCenter.right
}
My_Comp{
    id:decoration
    color:"darkGreen"
    width: 50
    height: 50
    anchors.horizontalCenter:recCenter.horizontalCenter
    anchors.verticalCenter:recCenter.verticalCenter
}
}
