import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    id:root
    visible: true
    width: 640
    height: 480
    color: "gray"
    title: qsTr("LIGHTER_FROM_HELL")
    
    property real activeOpacity:1
    property real inActiveOpacity:0.5
    
    Rectangle{
        id:traffic_lighter
        color: "black"
        width: 100
        height: 300
        radius: 20
        anchors.centerIn:parent
        state:"stop"
        Column{
            anchors.centerIn:parent
            spacing: 20
            Rectangle{
                id: rect_red
                color: "red"
                width: 75
                height: 75
                radius: 50
                
            }
            Rectangle{
                id: rect_yellow
                color: "yellow"
                width: 75
                height: 75
                radius: 50
            }
            Rectangle{
                id: rect_green
                color: "green"
                width: 75
                height: 75
                radius: 50
            }
        }
        
        
        states:[
            State {
                name: "stop"
                PropertyChanges {target: rect_red; opacity:activeOpacity}
                PropertyChanges {target: rect_yellow; opacity:inActiveOpacity}
                PropertyChanges {target: rect_green; opacity:inActiveOpacity}
            },
            State {
                name: "ready"
                PropertyChanges {target: rect_red; opacity:inActiveOpacity}
                PropertyChanges {target: rect_yellow; opacity:activeOpacity}
                PropertyChanges {target: rect_green; opacity:inActiveOpacity}
            },
            State {
                name: "go"
                PropertyChanges {target: rect_red; opacity:inActiveOpacity}
                PropertyChanges {target: rect_yellow; opacity:inActiveOpacity}
                PropertyChanges {target: rect_green; opacity:activeOpacity}
            }
        ]
        
        
        MouseArea{
            anchors.fill:parent
            onClicked: {
                if (parent.state == "stop") {
                    parent.state = "ready";
                } else if (parent.state == "ready") {
                    parent.state = "go";
                } else {
                    parent.state = "stop";
                }
            }
        }
        
        
        transitions:[
            Transition {
                from: "stop"
                to: "ready"
                PropertyAnimation{
                    target:rect_red; property: "opacity";from:inActiveOpacity; to: activeOpacity; duration: 1200}},
            Transition {
                from: "ready"
                to: "go"
                PropertyAnimation{
                    target:rect_yellow; property: "opacity";from:inActiveOpacity; to: activeOpacity; duration: 1200}},
            Transition {
                from: "go"
                to: "stop"
                PropertyAnimation{
                    target:rect_yellow; property: "opacity";from:inActiveOpacity; to: activeOpacity; duration: 1200}}
        ]
        
    }
}
