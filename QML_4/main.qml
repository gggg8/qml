import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    width: 360
    height: 640
    Rectangle{
        id:parRectangle
        anchors.fill: parent
        gradient:Gradient{
            GradientStop{position:0;color:"green"}
            GradientStop{position:1;color:"white"}
    }
    }
    title: "Messenger"

    ListModel {
        id: messageModel

        ListElement { message: "Hello" }
        ListElement { message: "Hi there" }
    }

    ListView {
        anchors.fill: parent
        model: messageModel
        delegate: Text {
            text: model.message
            font.bold: true
            color:black
        }
    }

    RowLayout {
        anchors.bottom: parent.bottom
        spacing: 10

        Rectangle{
            id:recText
            height:60
            width:parRectangle.width
            //anchors.fill:parent
            color:"grey"

            TextField {
                id: messageInput
                anchors.left:parent.left
                width:parent.width - 60
                placeholderText: "Message"
                Layout.fillWidth: true
            }

        Button {
            text: "Send"
            width:60
            anchors.right:parent.right
            onClicked: {
                messageModel.append({message: messageInput.text})
                messageInput.text = ""            }
        }
        }
    }
}

