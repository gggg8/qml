import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow {
    visible: true
    width: 540
    height: 880
   // title: "Footer Example"
    title: qsTr("MouseArea_Signals")

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            Layout.fillWidth: true
            Rectangle {
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    id: text_header
                    text: "Header"
                    anchors.centerIn: parent
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "lightgreen"
            Text {
                id: main_content
                text: "Content 0"
                anchors.centerIn: parent
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Rectangle {
                id:btn1
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    id:text_btn1
                    text: "Footer Content 1"
                    color:black
                    anchors.centerIn: parent
                }
                MouseArea {
                            id:mouse1
                            hoverEnabled: true
                            anchors.fill: btn1
                            onPressed: {btn1.width+=20;btn1.height+=20;
                            btn1.color="darkred"}
                            onReleased: {btn1.width-=20; btn1.height-=20;
                            text_btn1.color = "black";
                            text_btn2.color = "gray";
                            text_btn3.color = "gray";
                            text_header.text = "Header1"
                            main_content.text = "Content1"
                            }
                            onEntered: btn1.color="gray"
                            onExited: btn1.color="lightgray"
                }
            }


            Rectangle {
                id:btn2
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    id:text_btn2
                    text: "Footer Content 2"
                    color:black
                    anchors.centerIn: parent
                }
                MouseArea {
                            id:mouse2
                            hoverEnabled: true
                            anchors.fill: btn2
                            onPressed: {btn2.width+=20;btn2.height+=20;
                            btn2.color="darkred"}
                            onReleased: {btn2.width-=20; btn2.height-=20;
                            text_btn2.color = "black";
                            text_btn3.color = "gray";
                            text_btn1.color = "gray"
                            text_header.text = "Header2"
                            main_content.text = "Content2"
                            }
                            onEntered: btn2.color="gray"
                            onExited: btn2.color="lightgray"
                }
            }
            Rectangle {
                id:btn3
                Layout.fillWidth: true
                height: 50
                color: "lightgray"
                Text {
                    id:text_btn3
                    text: "Footer Content 3"
                    color:black
                    anchors.centerIn: parent
                }
                MouseArea {
                            id:mouse3
                            hoverEnabled: true
                            anchors.fill: btn3
                            onPressed: {btn3.width+=20;btn3.height+=20;
                            btn3.color="darkred"}
                            onReleased: {btn3.width-=20; btn3.height-=20;
                            text_btn3.color = "black";
                            text_btn2.color = "gray";
                            text_btn1.color = "gray"
                            text_header.text = "Header3"
                            main_content.text = "Content3"
                            }
                            onEntered: btn3.color="gray"
                            onExited: btn3.color="lightgray"
                }
            }
        }
    }
}
